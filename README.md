# Full-Stack Javascript Developer Coding Challenge

## Table of Content

* [Getting Started](#markdown-header-getting-started)
    * [Prerequisites](#markdown-header-prerequisites)
    * [Setup](#markdown-header-setup)
* [Challenge Description](#markdown-header-challenge-description)
    * [Front Office](#markdown-header-prerequisites)
    * [Back Office - Dispatcher dashboard](#markdown-header-back-office-dispatcher-dashboard)
    * [Pricing model](#markdown-header-pricing-model)
    * [Implementation requirements](#markdown-header-implementation-requirements)
    * [Deployment](#markdown-header-deployment)
* [Challenge Bonuses](#markdown-header-challenge-bonuses)
* [Evaluation](#markdown-header-Evaluation)
* [Submission Guidelines](#markdown-header-submission-guidelines)

## Getting Started

### Prerequisites

* [Git](http://git-scm.com/)
* [Node.js](https://nodejs.org/)
* [React](https://reactjs.org/)
* [Google's Distance Matrix Service](https://developers.google.com/maps/documentation/javascript/distancematrix)
* [MySQL](https://www.mysql.com/) or any other relational database
* [LoopBack](https://loopback.io/) or any other Node.js ORM framework

### Setup

* [Fork](https://bitbucket.org/iolap-adst/iolap-amb-fullstack-challenge/fork) or clone this repository: [iolap-adst/iolap-amb-fullstack-challenge](https://bitbucket.org/iolap-adst/iolap-amb-fullstack-challenge/)
* Setup your working environment
* Generate your backend Node.js and React projects ([Create React App](https://github.com/facebook/create-react-app))

## Challenge Description

The application serves the purpose of **ordering a taxi online**.

### Front Office

Guest user (client) opens a homepage and is represented with a form:

* Pickup location (origin address)
* Destination address
* Time and Date of pickup
* Phone number
* Email address (optional)
* Order button

Expected fare price must be displayed to the user before the actual order happens.

### Back Office - Dispatcher dashboard

Dispatcher should be able to login to the dashboard with his credentials.
On the dashboard, list of all orders should be shown in form of a table. Expected table columns are:

* Order ID
* Order Status
* Time and Date
* Origin
* Destination
* Fare price
* A button for displaying a detailed view of the order

In a detailed view of the order, dispatcher should be able to see all the information regarding that order:

* All the info in the table mentioned above
* Phone number
* Email address (if submitted)
* Distance between origin and destination points (Provided by [Google's Distance Matrix Service](https://developers.google.com/maps/documentation/javascript/distancematrix))
* Expected duration of the trip (Provided by [Google's Distance Matrix Service](https://developers.google.com/maps/documentation/javascript/distancematrix))

Additionally, *Order Status* should be changeable by dispatcher. Possible statuses are:

* Requested
* Declined
* In progress
* Done

### Pricing model

Prices will be calculated based on the distance provided by [Google's Distance Matrix Service](https://developers.google.com/maps/documentation/javascript/distancematrix).

If the distance **is 3 miles or less** (*base distance*), fare price is **fixed to $3** (*base price*). If the distance **is greater then 3 miles**, every *additional mile* will cost **$1 more**.

### Implementation requirements

* Frontend side (both back office and front office) should be implemented with [React](https://reactjs.org/) (you are encouraged to use additional libraries such as [Redux](https://redux.js.org/))
* Backend side should be using [LoopBack API Framework](https://loopback.io/) or any other Node.js ORM framework.
* Database should be a [MySQL](https://www.mysql.com/) or any other relational database.

### Deployment

Final version of your application should be deployed, visible and usable. You are free to use AWS, Azure or Heroku.

## Challenge Bonuses

Optional features:

* Test coverage
* Linting
* Error handling and validations
* Responsive front office UI
* Ability for dispatcher to change *base distance*, *base price* and *additional cost*.
* Sending Email with order details to a user (if email was provided)
* Whatever interesting you'd like to do

## Evaluation

Levels of functionality:

* **basic** - items mentioned in *Challenge Description*
* **above average** - some optional features mentioned in *Challenge Bonuses*
* **exceptional** - all of the above + CI/CD on AWS, Azure or Heroku

Application should support only the latest version of [Google Chrome](https://www.google.com/chrome/). Supporting other browsers is **not** required.

Our goal is to find answers to these questions:

* Do you understand the JavaScript language and more in general web technologies?
* Do you understand the chosen stack, framework, and MVC pattern in general?
* Can you design interfaces that are clear and easy to use?
* Can you judge which library/framework is the best fit for a job and use it correctly?
* Do you master your working environment?

## Submission Guidelines

Please let your iOLAP contact know when you're finished and provide:

* URL to your repository (it has to be publicly visible)
* URL to a deployed application
* Dispatcher user credentials for your deployed application
